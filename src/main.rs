use gtk::prelude::*;
use gtk::{Application, Window, glib, gio};
use adw::init;

const APP_ID: &str = "earth.flip.FlipEvents";

fn main() -> glib::ExitCode {
    gio::resources_register_include!("flip-events.gresource")
        .expect("Failed to register resources.");

    let app = Application::builder().application_id(APP_ID).build();
    app.connect_activate(build_ui);

    app.run()
}

fn build_ui(app: &Application) {
    let window = Window::new(app);
    init();
    window.present();
}
