# FLiP Events

Copyright © 2023 Andrew "HER0" Conrad

**This is not yet fully functional, and it is possible that it will never be.** Development is currently taking place on the `relm4` branch ([view online here](https://gitlab.com/FriendlyLinuxPlayers/applications/flip-events/-/tree/relm4?ref_type=heads)).

FLiP Events is an experimental application to make joining gaming events hosted by the [Friendly Linux Players](https://friendlylinuxplayers.org) community more accessible. The goal is to have a single application that can display events, join the Mumble voice chat server, and participate in the main Matrix chat room, while being easy to use and lightweight.

This code is freely available on
[GitLab](https://gitlab.com/FriendlyLinuxPlayers/applications/flip-events) under the terms of
the GNU GPL version 3. See `LICENSE.md` for details.
