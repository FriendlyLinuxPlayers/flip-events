using Gtk 4.0;
using Adw 1;

Window app_window {
  title: _("FLiP Events");
  titlebar: Adw.HeaderBar {
    centering-policy: strict;
    title-widget: Adw.ViewSwitcherTitle title {
      title: _("FLiP Events");
      //subtitle: _("Join gaming events hosted by the Friendly Linux Players");
      stack: main_stack;
    };
    Button toggle_calendar {
      visible: bind events_flap.folded;
      icon-name: "month-symbolic";
    }
    Button toggle_voice_messages {
     visible: bind voice_flap.folded;
     icon-name: "chat-bubble-text-symbolic";
    }
    [end]
    MenuButton {
      menu-model: app_menu;
      icon-name: "open-menu-symbolic";
      primary: true;
    }
  };
  child: Box {
    orientation: vertical;
    Adw.ViewStack main_stack {
      vexpand: true;
      Adw.ViewStackPage {
        name: "events";
        title: _("Events");
        icon-name: "gamepad-symbolic";
        child: Stack {
          Adw.StatusPage events_status {
            icon-name: "horizontal-arrows-one-way-symbolic";
            title: _("Network Error");
            description: _("Unable to download event data.");
            child: Button {
              styles ["suggested-action", "pill"]
              halign: center;
              width-request: 200;
              receives-default: true;
              label: _("Retry");
            };
          }
          Box events_box {
            orientation: vertical;
            Adw.Flap events_flap {
            vexpand: true;
              content: ScrolledWindow {
                hscrollbar-policy: never;
                Box {
                  orientation: vertical;
                  Label { label: "Ongoing or next event here, but fancier"; }
                  ListBox {
                  }
                }
              };
              flap: Box {
                styles ["background"]
                orientation: vertical;
                Calendar {}
              };
             //separator: Separator {};
            }
          }
        };
      }
      Adw.ViewStackPage {
        name: "voice";
        title: _("Voice");
        icon-name: "headset-symbolic";
        child: Stack {
          Adw.StatusPage voice_status {
            icon-name: "headset-symbolic";
            title: "Join Voice Chat";
            description: "Start chatting with others in the Mumble server for the Friendly Linux Players community.";
            child: Adw.Squeezer {
              Box {
                vexpand: false;
                halign: center;
                valign: center;
                spacing: 10;
                Label channel_selector_label {
                  label: _("Initial channel:");
                }
                ComboBoxText voice_channel_combo {
                  items [
                    events: "Events",
                    lobby: "Lobby",
                  ]
                  active-id: "events";
                }
                Button {
                  styles ["suggested-action", "pill"]
                  width-request: 200;
                  receives-default: true;
                  label: _("Join");
                }
              }
              Box {
                orientation: vertical;
                halign: center;
                spacing: 10;
                Box {
                  spacing: 20;
                  Label {
                    label: _("Initial channel:");
                  }
                  ComboBoxText {
                    items [
                      events: "Events",
                      lobby: "Lobby",
                    ]
                    active-id: "events";
                  }
                }
                Button {
                  styles ["suggested-action", "pill"]
                  receives-default: true;
                  label: _("Join");
                }
              }
            };
          }
          Adw.Flap voice_flap {
            content: Box {
              orientation: vertical;
              hexpand: true;
              ScrolledWindow {
                ListBox voice_list {
                  vexpand: true;
                  valign: start;
                }
              }
              Button ptt_button {
                styles ["suggested-action"]
                valign: end;
                label: "Push To Talk";
              }
            };
            flap: Box {
              styles ["background"]
              orientation: vertical;
              ScrolledWindow {
              hscrollbar-policy: never;
                ListBox voice_messages_list {
                 vexpand: true;
                  valign: fill;
                  //Label {label: "Joined the Events channel of the mumble.flip.earth voice chat server.";}
                }
              }
              Box {
                Button {
                  styles ["suggested-action"]
                  visible: bind voice_flap.folded;
                  icon-name: "microphone2";
                }
                Entry voice_entry {
                  hexpand: true;
                  placeholder-text: "Send message in Events";
                  input-hints: spellcheck;
                }
              }
            };
            separator: Separator {};
          }
        };
      }
      Adw.ViewStackPage {
        name: "messaging";
        title: _("Messaging");
        icon-name: "chat-symbolic";
        child: Adw.Leaflet {};
      }
    }
    Adw.ViewSwitcherBar {
      stack: main_stack;
      reveal: bind title.title-visible;
    }
  };
}

ShortcutsWindow shortcuts_window {
  section-name: ShortcutsSection{
    ShortcutsGroup{
      title: _("General");
    }
  };
}

AboutDialog about_dialog {
  program-name: "FLiP Events";
  comments: "Participate in Linux gaming events hosted by the Friendly Linux Players community.";
  website-label: "Learn more about events";
  website: "https://friendlylinuxplayers.org/events";
  authors: "Andrew Conrad";
  license-type: gpl_3_0_only;
}

menu app_menu {
  section {
    item {
      label: _("Keyboard Shortcuts");
      action: "app.shortcuts";
    }

    item {
      label: _("About FLiP Events");
      action: "app.about";
    }

    item {
      label: _("Quit");
      action: "app.quit";
    }
  }
}
